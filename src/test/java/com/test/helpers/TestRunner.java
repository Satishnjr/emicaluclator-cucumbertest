package com.test.helpers;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

//@RunWith(Cucumber.class)
@CucumberOptions(features = "classpath:features/CalculateEMI.feature", plugin = {
		"pretty", "html:target/cucumber-html-report", "json:cucumber.json" }, tags = {}, glue = {"com.test.helpers","com.test.step_definitions"})
public class TestRunner extends AbstractTestNGCucumberTests {
 
}
